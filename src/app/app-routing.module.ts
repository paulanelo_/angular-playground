import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NestedFormComponent } from './nested-form/nested-form.component';
import { VideoagularComponent } from './videoagular/videoagular.component';

const routes: Routes = [
  { path: '', component: VideoagularComponent },
  { path: 'nested-form', component: NestedFormComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
