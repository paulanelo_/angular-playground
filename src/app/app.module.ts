import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { VgCoreModule } from '@videogular/ngx-videogular/core';
import { VgControlsModule } from '@videogular/ngx-videogular/controls';
import { VgOverlayPlayModule } from '@videogular/ngx-videogular/overlay-play';
import { VgBufferingModule } from '@videogular/ngx-videogular/buffering';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NestedFormComponent } from './nested-form/nested-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { VideoagularComponent } from './videoagular/videoagular.component';

@NgModule({
  declarations: [AppComponent, NestedFormComponent, VideoagularComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
