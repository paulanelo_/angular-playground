import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-nested-form',
  templateUrl: './nested-form.component.html',
  styleUrls: ['./nested-form.component.scss'],
})
export class NestedFormComponent implements OnInit {
  form: FormGroup;
  themeList = [
    {
      id: 1007,
      type_id: 1,
      title:
        'O reflexo da tecnologia no mercado de trabalho e as novas profissões',
      slug: null,
      cover: null,
      category_id: null,
      created_at: '2020-12-22T18:50:46.000000Z',
      updated_at: '2020-12-22T18:50:46.000000Z',
    },
    {
      id: 1000,
      type_id: 1,
      title: 'fsfkdslkkfsflkdjsklfsjjfkjkfsjklfsj kfdsjkfdskj',
      slug: null,
      cover: null,
      category_id: null,
      created_at: '2020-12-22T18:50:46.000000Z',
      updated_at: '2020-12-22T18:50:46.000000Z',
    },
  ];

  constructor(private formBuider: FormBuilder) {
    this.form = this.formBuider.group({
      step: this.formBuider.group({
        partnership: this.formBuider.group({
          redacao: this.formBuider.group({
            essayThemeId: ['', Validators.nullValidator],
            theme: ['', Validators.nullValidator],
          }),
        }),
      }),
    });
  }

  handleForm() {
    console.log(this.form.value.step.partnership.redacao);
  }

  themeChange(e) {
    const { id } = this.themeList.find(
      (theme) => theme.title === e.target.value
    );
    this.essayThemeId.setValue(id);
  }

  get essayThemeId() {
    return this.form.get('step.partnership.redacao.essayThemeId');
  }

  ngOnInit(): void {}
}
