import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-videoagular',
  templateUrl: './videoagular.component.html',
  styleUrls: ['./videoagular.component.scss']
})
export class VideoagularComponent implements OnInit {
  @Input() videoUrl = 'http://static.videogular.com/assets/videos/videogular.mp4';

  constructor() { }

  ngOnInit(): void {
  }

}
