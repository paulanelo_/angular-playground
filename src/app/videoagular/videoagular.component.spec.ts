import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoagularComponent } from './videoagular.component';

describe('VideoagularComponent', () => {
  let component: VideoagularComponent;
  let fixture: ComponentFixture<VideoagularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideoagularComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoagularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
